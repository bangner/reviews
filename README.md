# Lou's Reviews Docs

## Challenge Goal
The goal of the project was to use Birdeye's API to retrieve reviews for the provided `businessId` and store these responses in a database as well as provide a simple front end to view these reviews.

## Approach

Before building this app, I read through Birdeye's documentation and tested a few calls with the provided `businessId` and `api_key`. I noticed another endpoint `GET reviews summary` and used that to get some a quick glance at the `total_count` and `sources` of reviews. This allowed me to confirm that I was receiving the full response from Birdeye when using the `GET reviews` endpoint.

## Solution
This is a Sinatra app with a PostgreSQL database. I chose to use Sinatra over Ruby on Rails since RoR comes with a lot of boilerplate code, much of it is unnecessary for this challenge. I used PostgreSQL so I could easily deploy to Heroku.

The app is very simple, with only one model and three views. Most of the logic is in the `POST /poll` method located in `app.rb`. When I started the project, Birdeye was returning 93 reviews. Due to this number of records, I set the `count` parameter to 200, just in case this location gets an influx of reviews over the next few days :)

## Possible Features
1. Allow the user to sort based on Star rating, date, etc.
2. Set up a schedule that would fetch reviews every X hours.
3. Separate the `customers` from the review table. I decided to leave this as one table due to the instructions stating "a table will need to be created within the database". In addition, after looking at the customers and verifying this is for one location, I didn't see the need to separate customers into their own table. If we were pulling reviews for all locations, we could possibly have a customer that's left two or more reviews and therefore would be nice to be able to have a customer resource.


## Setup and Installation
1. Clone this repository from [BitBucket](https://bitbucket.org/bangner/reviews/overview)
2. Run `bundle install`
3. Run `rake db:create db:migrate`
4. Run `rackup -p 4567`
5. When running locally, your local database will not be populated. Please make sure you click "Poll For New Reviews" or visit [/polls](http://localhost:4567/poll)

This project is also hosted on Heroku and can be found [here](https://shielded-shelf-45076.herokuapp.com/)


