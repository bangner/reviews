require 'sinatra'
require 'sinatra/activerecord'
require './models/review'
require 'json'
require 'rest-client'

# GET /
# -----
# Welcome Page
get '/' do
  erb :welcome
end

# GET /reviews
# ------------
# Index page for all reviews
get '/reviews/?' do
  @reviews = Review.all
  erb :'reviews/index'
end

# GET /review/:id
# ---------------
# Show page for Reviews
get '/reviews/:id/?' do
  @review = Review.find params[:id]
  erb :'reviews/show'
end

# GET /poll
# ---------
# Polls Birdeye for all Reviews and find or create a Review record.
get '/poll/?' do
  response = RestClient.post("https://api.birdeye.com/resources/v1/review/businessId/149440583942847?sindex=0&count=200&api_key=duRQ6T9xcAoSPGSrpi5Cgzazrd165ziD",
                             '{"ratings":[1,2,3,4,5,0]}',
                             {content_type: :json, accept: :json}
                            )

  reviews = JSON.parse(response)

  reviews.each do |review|
    Review.find_or_create_by(review_id:review["reviewId"],
                             rating: review["rating"],
                             title: review["title"],
                             review_url: review["reviewUrl"],
                             source_type: review["sourceType"],
                             review_date: review["reviewDate"],
                             comment: review["comments"],
                             response: review["response"],
                             response_date: review["responseDate"],
                             unique_review_url: review["uniqueReviewUrl"],
                             customer_nick_name: review["reviewer"]["nickName"],
                             customer_first_name: review["reviewer"]["firstName"],
                             customer_last_name: review["reviewer"]["lastName"],
                             customer_avatar: review["reviewer"]["thumbnailUrl"],
                             customer_id: review["reviewer"]["customerId"],
                             customer_email_id: review["reviewer"]["emailId"],
                             customer_facebook_id: review["reviewer"]["facebookId"],
                             customer_city: review["reviewer"]["city"],
                             customer_state: review["reviewer"]["state"],
                             tags: review["tags"],
                             business_type: review["businessName"],
                             business_name: review["businessType"],
                             featured: review["featured"]
                            )
  end
  redirect '/reviews'
end