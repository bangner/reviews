class CreateReviews < ActiveRecord::Migration[5.1]
  def change
    create_table :reviews do |t|
      t.string   :review_id
      t.float    :rating
      t.string   :title
      t.text     :comment
      t.string   :review_url
      t.string   :source_type
      t.date     :review_date
      t.text     :response
      t.date     :response_date
      t.string   :unique_review_url
      t.string   :customer_first_name
      t.string   :customer_last_name
      t.string   :customer_nick_name
      t.string   :customer_avatar
      t.string   :customer_email_id
      t.string   :customer_facebook_id
      t.string   :customer_city
      t.string   :customer_state
      t.string   :customer_id
      t.string   :tags
      t.string   :business_type
      t.string   :business_name
      t.boolean  :featured, default: false


      t.timestamps
    end
    add_index :reviews, :review_id, unique: true
  end
end
